class Query {
  constructor(q, r, n) {
    this._req = q
    this._res = r
    this._nxt = n
  }

  get get() { return this._req.query }

  send() {
    this._res.send(...arguments)
    this._nxt()
  }
}

module.exports = Query
