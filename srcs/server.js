const fs = require("fs")
const path = require("path")
const restify = require("restify")
const corsMiddleware = require('restify-cors-middleware')
const Query = require("./query")

class Server {
  constructor(config) {
    this.server = restify.createServer()
    this.server.use(restify.plugins.queryParser())
 
    if (!config || !config.cors) return
    const cors = corsMiddleware(config.cors)
 
    this.server.pre(cors.preflight)
    this.server.use(cors.actual)
  }

  load(base, baseroute = "") {
    const files = fs.readdirSync(base)
    for (const file of files) {
      const fullPath = path.join(base, file)
      const stats = fs.statSync(fullPath)
      if (stats.isDirectory()) {
        this.load(fullPath, `${baseroute}/${file}`)
        continue
      }

      const name = path.basename(fullPath, ".js")
      const module = require(fullPath)
      const route = `${baseroute}/${name}`
      if (module.get) this.get(route, ...module.get)
    }
  }

  get(route, ...handlers) {
    this.server.get(route, ...handlers.map(h => (r, q, n) => h(new Query(r, q, n))))
  }

  listen() { return this.server.listen(...arguments) }
  address() { return this.server.address(...arguments) }
}

module.exports = Server